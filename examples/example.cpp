#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>

int main()
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

	::qifen_qi_context_t ctx;
	::qifen_qi_t a, b, c;
	double x, y;
	::kv::affine<double> affine, affine2;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup_string(ctx, a, "0.5", "0.7");
	::qifen_qi_init_infsup_string(ctx, b, "0.3", "0.7");
	::qifen_qi_init(ctx, c);

	::qifen_qi_sub(ctx, c, a, b);
	::qifen_qi_get_infsup(ctx, c, &x, &y);

	::std::cout << "[0.5, 0.7] - [0.3, 0.7]" << ::std::endl;
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << ::std::endl;

	ctx->num_dummy = 0;

	::qifen_qi_set_infsup_string(ctx, a, "0.2", "1.2");
	::std::cout << "1 / [0.2, 1.2]" << ::std::endl;
	::qifen_qi_kv_inv(ctx, c, a, { ::qifen_approx_remez, 10 });
	::qifen_qi_get_infsup(ctx, c, &x, &y);

	affine = ::kv::interval<double>("0.2", "1.2");
	affine = 1.0 / affine;
	::kv::affine<double>::maxnum() = 0;

	::std::cout << "kv::interval " << (1.0 / ::kv::interval<double>("0.2", "1.2")) << std::endl;
	::std::cout << "kv::affine   " << to_interval(affine) << std::endl;
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << ::std::endl;

	ctx->num_dummy = 0;

	::qifen_qi_set_infsup_string(ctx, a, "1.1", "1.2");

	::std::cout << "1 / [1.1, 1.2]" << ::std::endl;
	::qifen_qi_kv_inv(ctx, c, a, { ::qifen_approx_linear, 10 });
	::qifen_qi_get_infsup(ctx, c, &x, &y);

	affine = ::kv::interval<double>("1.1", "1.2");
	affine = 1.0 / affine;

	::std::cout << "kv::interval " << (1.0 / ::kv::interval<double>("1.1", "1.2")) << std::endl;
	::std::cout << "kv::affine   " << to_interval(affine) << std::endl;
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << ::std::endl;

	::std::cout << "1 / (1 / [1.1, 1.2])" << ::std::endl;
	::qifen_qi_kv_inv(ctx, c, c, { ::qifen_approx_remez, 10 });
	::qifen_qi_get_infsup(ctx, c, &x, &y);
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << ::std::endl;

	ctx->num_dummy = 0;
	::qifen_qi_set_infsup(ctx, a, 2.0, 2.0 + 2.0);

	affine = ::kv::interval<double>(2.0, 2.0 + 2.0);
	affine2 = 1.0 / affine;
	auto affine3 = affine * affine2;

	::std::cout << "1 / [2.0, 4.0]" << ::std::endl;
	::qifen_qi_kv_inv(ctx, b, a, { ::qifen_approx_remez, 40 });
	::qifen_qi_get_infsup(ctx, b, &x, &y);
	::std::cout << "kv::affine   " << to_interval(affine2) << std::endl;
	::std::cout << "x0 = " << b->x0 << ::std::endl;
	::std::cout << "a  = " << **b->a << ::std::endl;
	::std::cout << "A  = " << **b->A << ::std::endl;
	::std::cout << "delta = " << b->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << ::std::endl;

	::std::cout << "[2.0, 4.0] * (1 / [2.0, 4.0])" << ::std::endl;
	::qifen_qi_kv_mul(ctx, c, a, b);
	::qifen_qi_get_infsup(ctx, c, &x, &y);
	::std::cout << "kv::affine   " << to_interval(affine3) << std::endl;
	::std::cout << "kv::affine   " << affine << std::endl;
	::std::cout << "kv::affine   " << affine2 << std::endl;
	::std::cout << "kv::affine   " << affine3 << std::endl;
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << "qi rad " << (y - x) / 2.0 << ::std::endl;
	::std::cout << "kv rad " << rad(to_interval(affine3)) << ::std::endl;
	::std::cout << ::std::endl;

	ctx->num_dummy = 0;
	::qifen_qi_set_infsup(ctx, a, 2.0, 2.0 + 2.0);
	::std::cout << "[1.0, 1.1] * (1 / [2.0, 4.0])" << ::std::endl;
	::qifen_qi_kv_inv(ctx, b, a, { ::qifen_approx_fast, 40 });
	::qifen_qi_kv_mul(ctx, c, a, b);
	::qifen_qi_get_infsup(ctx, c, &x, &y);
	::std::cout << "x0 = " << c->x0 << ::std::endl;
	::std::cout << "a  = " << **c->a << ::std::endl;
	::std::cout << "A  = " << **c->A << ::std::endl;
	::std::cout << "delta = " << c->delta << ::std::endl;
	::std::cout << "range: " << x << ' ' << y << ::std::endl;
	::std::cout << "qi rad " << (y - x) / 2.0 << ::std::endl;
	::std::cout << ::std::endl;

	::kv::affine<double>::maxnum() = 0;

	::qifen_qi_clear(ctx, a);
	::qifen_qi_clear(ctx, b);
	::qifen_qi_clear(ctx, c);
	::qifen_qi_context_clear(ctx);
}
