#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#define AFFINE_MULT 2

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <boost/scope_exit.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>
#include <cstdlib>

#include <qifen/internal.hpp>

::std::pair<double, double> calc_qifen(double epsilon, const ::qifen_qi_inv_config_struct &config)
{
	::qifen_qi_context_t ctx;
	::qifen_qi_t x, t1, t2, t3, f;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup(ctx, x, 1e4 - epsilon, 1e4 + epsilon);
	::qifen_qi_init(ctx, t1);
	::qifen_qi_init(ctx, t2);
	::qifen_qi_init(ctx, t3);
	::qifen_qi_init(ctx, f);

	BOOST_SCOPE_EXIT_ALL(&) {
		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, t1);
		::qifen_qi_clear(ctx, t2);
		::qifen_qi_clear(ctx, t3);
		::qifen_qi_clear(ctx, f);
		::qifen_qi_context_clear(ctx);
	};

	// t1 = x + 1.0
	::qifen_qi_add_scalar(ctx, t1, x, 1.0);
	// t2 = 1.0 / x
	if (::qifen_qi_inv(ctx, t2, x, config) != ::qifen_error_succeeded)
		return ::std::make_pair(0.0, 0.0);
	// t3 = 1.0 / (x+1.0)
	if (::qifen_qi_inv(ctx, t3, t1, config) != ::qifen_error_succeeded)
		return ::std::make_pair(0.0, 0.0);
	// t1 = x * (x + 1.0)
	::qifen_qi_mul(ctx, t1, x, t1);
	// t2 = (1.0 / x) - (1.0 / (x+1.0))
	::qifen_qi_sub(ctx, t2, t2, t3);
	::qifen_qi_mul(ctx, x, t1, t2);

	// t1 = x * x
	::qifen_qi_mul(ctx, t1, x, x);
	// t2 = 2.0 * x
	::qifen_qi_mul_scalar(ctx, t2, x, 2.0);
	// f = x*x - 2.0*x
	::qifen_qi_sub(ctx, f, t1, t2);

	auto r = ::qifen_qi_get_infsup(ctx, f);

	if (-1.0 >= r.first && -1.0 <= r.second)
		return r;
	else
		return ::std::make_pair(1.0, 1.0);
}

int main(int argc, char **argv)
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

    double epsilon = 1e2;

    if (argc > 1) {
        epsilon = ::std::stod(argv[1]);
    }

	auto q = ::calc_qifen(epsilon, { ::qifen_approx_chebyshev });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	q = ::calc_qifen(epsilon, { ::qifen_approx_chebyshev_2 });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	q = ::calc_qifen(epsilon, { ::qifen_approx_remez, 10 });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';
	
	q = ::calc_qifen(epsilon, { ::qifen_approx_fast });
	::std::cout << q.first << ' ' << q.second << ' ' << (q.second - q.first) / 2.0 << ' ';

	::kv::affine<double> a, a2;

	a = ::kv::interval<double>(1e4 - epsilon, 1e4 + epsilon);
	a2 = a + 1.0;
	a2 = a * a2 * (1.0 / a - 1.0 / a2);
	a2 = pow(a2, 2) - 2.0 * a2;

	::std::cout << rad(to_interval(a2)) << ::std::endl;
	::std::cout << to_interval(a2) << ::std::endl;
}
