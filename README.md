# qifen

二次形式を使って区間演算をするライブラリ。

「**q**uadratic form を使った **i**nterval arithmetic」を略して qi。
漆 (中国語で qī、発音記号は [t͡ɕʰi˥]) の主成分がウルシオール (中国語で漆酚、拼音は qīfēn、発音記号は [t͡ɕʰi˥.fən˥])。
二次形式を使った区間演算 (qi) の基になるライブラリで qifen。

## ドキュメント

https://uec-dn.bitbucket.io/docs/qifen

## できること

- 一般的な区間演算
    - 内部で [kv](http://github.com/mskashi/kv) か [INTLAB](http://www.ti3.tu-harburg.de/rump/intlab/) を利用する
- 二次形式を使った区間演算 (一部未実装)

## 利用できる環境

少なくとも kv と INTLAB のどちらか片方が使える環境。INTLAB を利用する場合は MATLAB 上で [MEX ファイルの作成](https://jp.mathworks.com/help/matlab/matlab_external/introducing-mex-files.html)ができるようにしておく必要がある。

## 対応する言語

ライブラリのビルドは C++ で行う必要があるが、作成したライブラリは C 言語からでも C++ からでも利用できる。

## サンプルプログラムのビルド

[CMake](https://cmake.org/) を使うとビルドできる。
Windows であれば `build` ディレクトリで
```
cmake -G "Visual Studio 14 2015 Win64" ..
```
のようなコマンドを入力すると生成される `.sln` ファイルを開けば良い。

`submodules` ディレクトリの中が空の場合は
```
git submodule init 
git submodule update
```
を実行する必要がある。

## ビルド方法・利用方法

### kv のみ使える環境

#### ライブラリのビルド

`QIFEN_CONFIG_INTERVAL_KV` と `QIFEN_CONFIG_DISABLE_INTLAB` の二つのマクロを定義してビルドする。gcc や clang では `-D` オプション、Visual C++ では `/D` オプションを使う。また、Boost のヘッダファイルの場所、kv のヘッダファイルの場所、qifen のヘッダの場所の三つを指定する必要がある。gcc や clang では `-I` オプション、Visual C++ では `/I` オプションを使えば良い。Visual C++ では次のようなコマンドでビルドできる。

```
cl /W3 /EHsc /c /O2 /MD /Fo.\Release\interval.obj /IC:\local\boost_1_61_1 /I.\include /DQIFEN_CONFIG_INTERVAL_KV /DQIFEN_CONFIG_DISABLE_INTLAB qifen\interval.cpp
cl /W3 /EHsc /c /O2 /MD /Fo.\Release\matrix.obj /IC:\local\boost_1_61_1 /I.\include /DQIFEN_CONFIG_INTERVAL_KV /DQIFEN_CONFIG_DISABLE_INTLAB qifen\matrix.cpp
cl /W3 /EHsc /c /O2 /MD /Fo.\Release\interval_matrix.obj /IC:\local\boost_1_61_1 /I.\include /DQIFEN_CONFIG_INTERVAL_KV /DQIFEN_CONFIG_DISABLE_INTLAB qifen\interval_matrix.cpp
cl /W3 /EHsc /c /O2 /MD /Fo.\Release\qi.obj /IC:\local\boost_1_61_1 /I.\include /DQIFEN_CONFIG_INTERVAL_KV /DQIFEN_CONFIG_DISABLE_INTLAB qifen\qi.cpp
cl /W3 /EHsc /c /O2 /MD /Fo.\Release\qi_context.obj /IC:\local\boost_1_61_1 /I.\include /DQIFEN_CONFIG_INTERVAL_KV /DQIFEN_CONFIG_DISABLE_INTLAB qifen\qi_context.cpp
lib /out:.\Release\qifen.lib /NODEFAULTLIB .\Release\*.obj
```

#### ライブラリの利用

ビルドしたライブラリを使う場合は、`qifen.h` を include する前に `QIFEN_CONFIG_INTERVAL_KV` と `QIFEN_CONFIG_DISABLE_INTLAB` の二つのマクロを定義する必要がある。ソースファイルの頭の方に次の三行を書けば良い。

```c
#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB
#include <qifen.h>
```


### INTLAB のみ使える環境

#### ライブラリのビルド

`QIFEN_CONFIG_INTERVAL_INTLAB` と `QIFEN_CONFIG_DISABLE_KV` の二つのマクロを定義してビルドする。MATLAB で MEX ファイルを作るときに必要な `mex.h` は、ライブラリをビルドするときには必要ない。qifen のヘッダファイルの場所だけ指定する必要がある。

#### ライブラリの利用

`qifen.h` を include する前に `QIFEN_CONFIG_INTERVAL_INTLAB` と `QIFEN_CONFIG_DISABLE_KV` の二つのマクロを定義しておく必要がある。

### kv と INTLAB の両方が使える環境

#### ライブラリのビルド

ライブラリの内部で持つ区間を kv の区間型で保持する場合は `QIFEN_CONFIG_INTERVAL_KV` を、INTLAB の区間型で保持する場合は `QIFEN_CONFIG_INTERVAL_INTLAB` を定義してビルドする。Boost と kv と qifen のヘッダファイルの場所を指定する必要がある。

#### ライブラリの利用

`qifen.h` を include する前に、ライブラリをビルドした時と同じマクロを定義しておく必要がある。

