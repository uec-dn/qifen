#pragma once

#include <qifen/matrix.h>
#include <qifen/mex-compatible.h>

#include <boost/scope_exit.hpp>

namespace qifen {
	namespace internal {
#if defined(QIFEN_CONFIG_INTERVAL_KV)
		const ::boost::numeric::ublas::matrix<double> &matrix_to_kv(const ::qifen_matrix_t);
		::boost::numeric::ublas::matrix<double> &matrix_to_kv(::qifen_matrix_t);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
		::boost::numeric::ublas::matrix<double> matrix_to_kv(const ::qifen_matrix_t);
#endif
		::mxArray *matrix_to_intlab(const ::qifen_matrix_t);
		void matrix_set(::qifen_matrix_t, const ::boost::numeric::ublas::matrix<double> &);
		void matrix_set(::qifen_matrix_t, ::mxArray *);
	}
}

#define QIFEN_VAR_INTLAB_MATRIX(name, value) \
   ::mxArray *name = ::qifen::internal::matrix_to_intlab(value); \
   QIFEN_VAR_INTLAB_MATRIX_RELEASE(name)

#if defined(QIFEN_CONFIG_INTERVAL_KV)
#  define QIFEN_VAR_KV_MATRIX(name, value) \
     auto &name = ::qifen::internal::matrix_to_kv(value)
#  define QIFEN_VAR_INTLAB_MATRIX_RELEASE(name) BOOST_SCOPE_EXIT_ALL(&name){ ::mxDestroyArray(name); }
#  define QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest) \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest){ ::qifen::internal::interval_matrix_set(dest, lhs); ::mxDestroyArray(lhs); }
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#  define QIFEN_VAR_KV_MATRIX(name, value) \
     auto name = ::qifen::internal::matrix_to_kv(value)
#  define QIFEN_VAR_INTLAB_MATRIX_RELEASE QIFEN_NOOP
#  define QIFEN_MEX_LHS_AS_DEST_MATRIX(lhs, dest) \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest) { ::mxDestroyArray(dest->value); dest->value = lhs; }
#endif
