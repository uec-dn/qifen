#pragma once

namespace qifen {
	namespace internal {
		template <typename T>
		struct range {
			T *b, *e;

			T *begin()
			{
				return b;
			}

			T *end()
			{
				return e;
			}

			const T *begin() const
			{
				return b;
			}

			const T *end() const
			{
				return e;
			}
		};
	}
}
