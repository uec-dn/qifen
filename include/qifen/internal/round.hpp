#pragma once

#include <utility>
#include <limits>
#include <tuple>
#include <cmath>

#if defined(_MSC_VER)
#  include <float.h>
#else
#  include <cfenv>
#endif

namespace qifen {
	namespace internal {
		inline ::std::pair<double, double> split(double a)
		{
			volatile auto t = a * (::std::ldexp(1, 27) + 1);
			auto x = t - (t - a);
			auto y = a - x;
			return ::std::make_pair(x, y);
		}

		inline ::std::pair<double, double> twosum(double a, double b)
		{
			volatile auto x = a + b;
			double y;
			if (::std::abs(a) > ::std::abs(b)) {
				volatile auto t = x - a;
				y = b - t;
			} else {
				volatile auto t = x - b;
				y = a - t;
			}
			return ::std::make_pair(x, y);
		}

		inline ::std::pair<double, double> twoproduct(double a, double b)
		{
			const double c1 = ::std::ldexp(1, 996);
			const double c2 = ::std::ldexp(1, 28);
			const double c3 = 1.0 / c2;
			const double c4 = ::std::ldexp(1, -1023);
			auto x = a * b;
			double y, an = a, bn = b;

			if (::std::abs(a) > c1) {
				an = a * c3;
				bn = b * c2;
			} else if (::std::abs(b) > c1) {
				an = a * c2;
				bn = b * c3;
			}

			volatile double a1, a2, b1, b2;

			::std::tie(a1, a2) = split(an);
			::std::tie(b1, b2) = split(bn);

			if (::std::abs(x) > c4) {
				y = a2 * b2 - (((x * 0.5 - (a1 * 0.5) * b1) * 2.0 - a2 * b1) - a1 * b2);
			} else {
				y = a2 * b2 - (((x - a1 * b1) - a2 * b1) - a1 * b2);
			}

			return ::std::make_pair(x, y);
		}

		inline double add_up(double a, double b)
		{
			constexpr double inf = ::std::numeric_limits<double>::infinity();
			double x, y;

			::std::tie(x, y) = twosum(a, b);

			if (x == inf) {
				return inf;
			} else if (x == -inf) {
				if (a == -inf || b == -inf) {
					return x;
				} else {
					return -::std::numeric_limits<double>::max();
				}
			}

			if (y > 0) {
				return ::std::nextafter(x, inf);
			}

			return x;
		}

		inline double mul_up(double a, double b)
		{
			constexpr double inf = ::std::numeric_limits<double>::infinity();
			const double c1 = ::std::ldexp(1, -969);
			const double c2 = ::std::ldexp(1, 537);
			double x, y;

			::std::tie(x, y) = twoproduct(a, b);

			if (x == inf) {
				return x;
			} else if (x == -inf) {
				if (::std::abs(a) == inf || ::std::abs(b) == inf) {
					return x;
				} else {
					return -::std::numeric_limits<double>::max();
				}
			}

			if (::abs(a) >= c1) {
				if (y > 0.0) {
					return ::std::nextafter(x, inf);
				}
				return x;
			} else {
				double s, s2;

				::std::tie(s, s2) = twoproduct(a * c2, b * c2);
				auto t = (x * c2) * c2;

				if (t < s || (t == s && s2 > 0)) {
					return ::std::nextafter(x, inf);
				}

				return x;
			}
		}

		template <typename F>
		inline void round_to_up(F f)
		{
#if defined(_MSC_VER)
			unsigned int mode;
			::_controlfp_s(&mode, _RC_UP, _MCW_RC);
#else
			int mode;
			mode = ::std::fesetround(FE_UPWARD);
#endif
			f();
#if defined(_MSC_VER)
			::_controlfp_s(&mode, mode, _MCW_RC);
#else
			::std::fesetround(mode);
#endif
		}
	}
}