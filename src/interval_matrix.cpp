#include <cstring>
#include <new>
#include <algorithm>

#include <qifen/interval_matrix.h>
#include <qifen/interval-internal.hpp>
#include <qifen/interval_matrix-internal.hpp>
#include <qifen/matrix-internal.hpp>

#include <kv/matrix-inversion.hpp>
#include <kv/interval-vector.hpp>

using interval_t = ::qifen_interval_matrix_struct::interval_t;
using matrix_t = ::qifen_interval_matrix_struct::matrix_t;

const size_t qifen_sizeof_interval_matrix = sizeof(::qifen_interval_matrix_t);

namespace qifen {
	namespace internal {
#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
		const ::boost::numeric::ublas::matrix<::kv::interval<double>> &interval_matrix_to_kv(const ::qifen_interval_matrix_t v)
		{
			return **v;
		}
		::boost::numeric::ublas::matrix<::kv::interval<double>> &interval_matrix_to_kv(::qifen_interval_matrix_t v)
		{
			return **v;
		}
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
		::boost::numeric::ublas::matrix<::kv::interval<double>> interval_matrix_to_kv(const ::qifen_interval_matrix_t v)
		{
			::mxArray *inf = ::mxGetField(v->value, 0, "inf");
			::mxArray *sup = ::mxGetField(v->value, 0, "sup");
			size_t nrows = ::mxGetM(inf);
			size_t ncols = ::mxGetN(inf);
			auto pinf = ::mxGetPr(inf);
			auto psup = ::mxGetPr(sup);

			::boost::numeric::ublas::matrix<::kv::interval<double>> r(nrows, ncols);

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					r(j, i).lower() = pinf[j + i * nrows];
					r(j, i).upper() = psup[j + i * nrows];
				}
			}

			return r;
		}
#  endif
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		::mxArray *interval_matrix_to_intlab(const ::qifen_interval_matrix_t v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			auto &mat = **v;
			size_t nrows = mat.size1();
			size_t ncols = mat.size2();
			::mxArray *inf = ::mxCreateDoubleMatrix(nrows, ncols, mxREAL);
			::mxArray *sup = ::mxCreateDoubleMatrix(nrows, ncols, mxREAL);
			auto pinf = ::mxGetPr(inf);
			auto psup = ::mxGetPr(sup);

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					pinf[j + i * nrows] = mat(j, i).lower();
					psup[j + i * nrows] = mat(j, i).upper();
				}
			}

			::mxArray *lhs;
			::mxArray *rhs[] = { inf, sup };

			::mexCallMATLAB(1, &lhs, 2, rhs, "infsup");
			::mxDestroyArray(inf);
			::mxDestroyArray(sup);

			return lhs;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			return const_cast<::mxArray*>(v->value);
#  endif
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_KV)
		template <typename T>
		void interval_matrix_set(::qifen_interval_matrix_t dest, const ::boost::numeric::ublas::matrix_expression<T> &v)
		{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
			**dest = v;
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
			auto vv = v();
			auto nrows = vv.size1();
			auto ncols = vv.size2();
			auto inf = ::mxCreateDoubleMatrix(nrows, ncols, mxREAL);
			auto sup = ::mxCreateDoubleMatrix(nrows, ncols, mxREAL);
			auto pinf = ::mxGetPr(inf);
			auto psup = ::mxGetPr(sup);

			QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);
			::mxArray *rhs[] = { inf, sup };

			for (size_t i = 0; i < ncols; ++i) {
				for (size_t j = 0; j < nrows; ++j) {
					pinf[j + i * nrows] = vv(j, i).lower();
					psup[j + i * nrows] = vv(j, i).upper();
				}
			}

			::mexCallMATLAB(1, &lhs, 2, rhs, "infsup");
#  endif
		}

		void interval_matrix_set(::qifen_interval_matrix_t dest, const ::boost::numeric::ublas::matrix<::kv::interval<double>> &v)
		{
			interval_matrix_set<::boost::numeric::ublas::matrix<::kv::interval<double>>>(dest, v);
		}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
		void interval_matrix_set(::qifen_interval_matrix_t dest, ::mxArray *v)
		{
			QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);
			::mexCallMATLAB(1, &lhs, 1, &v, "intval");
		}
#endif
	}
}

void qifen_interval_matrix_init(qifen_interval_matrix_t v, size_t nrow, size_t ncol)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	new(&v->storage) matrix_t(nrow, ncol);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxArray *rhs = ::mxCreateDoubleMatrix(nrow, ncol, mxREAL);

	::mexCallMATLAB(1, &v->value, 1, &rhs, "intval");
	::mxDestroyArray(rhs);
#endif
}

void qifen_interval_matrix_clear(qifen_interval_matrix_t v)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*v)->~matrix_t();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxDestroyArray(v->value);
#endif
}

void qifen_interval_matrix_set(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	**dest = **a;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxDestroyArray(dest->value);
	::mexCallMATLAB(1, &dest->value, 1, const_cast<mxArray**>(&a->value), "intval");
#endif
}

::qifen_error_t qifen_interval_matrix_set_infsup(qifen_interval_matrix_t dest, const qifen_matrix_t inf, const qifen_matrix_t sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (inf == nullptr && sup == nullptr) {
		return ::qifen_error_succeeded;
	}if (inf == nullptr) {
		**dest = **sup;
		return ::qifen_error_succeeded;
	} else if (sup == nullptr) {
		**dest = **inf;
		return ::qifen_error_succeeded;
	} else {
		auto &d = **dest;
		auto &a = **inf;
		auto &b = **sup;
		auto nrows = a.size1();
		auto ncols = a.size2();

		if (nrows != b.size1() || ncols != b.size2()) {
			return ::qifen_error_matrix_size;
		}

		d.resize(nrows, ncols, false);

		for (size_t i = 0; i < nrows; ++i) {
			for (size_t j = 0; j < ncols; ++j) {
				d(i, j).lower() = a(i, j);
				d(i, j).upper() = b(i, j);
			}
		}

		return ::qifen_error_succeeded;
	}
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::mxDestroyArray(dest->value);

	if (inf == nullptr && sup == nullptr) {
		return ::qifen_error_succeeded;
	} else if (inf == nullptr) {
		::mexCallMATLAB(1, &dest->value, 1, const_cast<::mxArray**>(&sup->value), "intval");
	} else if (sup == nullptr) {
		::mexCallMATLAB(1, &dest->value, 1, const_cast<::mxArray**>(&inf->value), "intval");
		return ::qifen_error_succeeded;
	} else {
		if (::mxGetM(inf->value) != ::mxGetM(sup->value) || ::mxGetN(inf->value) != ::mxGetN(sup->value)) {
			return ::qifen_error_matrix_size;
		}

		::mxArray *rhs[] = { const_cast<::mxArray**>(&inf->value), const_cast<::mxArray**>(&sup->value) };
		::mexCallMALAB(1, &dest->value, 2, rhs, "infsup");

		return ::qifen_error_succeeded;
	}
#endif
}

void qifen_interval_matrix_set_entry(qifen_interval_matrix_t dest, size_t row, size_t col, qifen_interval_t v)
{
	double inf, sup;
	::qifen_interval_get_infsup(v, &inf, &sup);
	::qifen_interval_matrix_set_entry_infsup(dest, row, col, inf, sup);
}

void qifen_interval_matrix_set_entry_infsup(qifen_interval_matrix_t dest, size_t row, size_t col, double inf, double sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	auto &d = (**dest)(row, col);
	d.lower() = inf;
	d.upper() = sup;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	auto pinf = ::mxGetField(dest->value, 0, "inf");
	auto psup = ::mxGetField(dest->value, 0, "sup");
	auto nrows = ::mxGetM(pinf);
	::mxGetPr(pinf)[row + col * nrows] = inf;
	::mxGetPr(psup)[row + col * nrows] = sup;
#endif
}

void qifen_interval_matrix_get_infsup(const qifen_interval_matrix_t dest, qifen_matrix_t inf, qifen_matrix_t sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (inf != nullptr && sup != nullptr) {
		auto &d = **dest;
		auto &a = **inf;
		auto &b = **sup;
		auto nrows = d.size1();
		auto ncols = d.size2();

		a.resize(nrows, ncols, false);
		b.resize(nrows, ncols, false);

		for (size_t i = 0; i < nrows; ++i) {
			for (size_t j = 0; j < ncols; ++j) {
				a(i, j) = d(i, j).lower();
				b(i, j) = d(i, j).upper();
			}
		}
	} else if (inf == nullptr) {
		auto &d = **dest;
		auto &b = **sup;
		auto nrows = d.size1();
		auto ncols = d.size2();

		b.resize(nrows, ncols, false);

		for (size_t i = 0; i < nrows; ++i) {
			for (size_t j = 0; j < ncols; ++j) {
				b(i, j) = d(i, j).upper();
			}
		}
	} else if (sup == nullptr) {
		auto &d = **dest;
		auto &a = **inf;
		auto nrows = d.size1();
		auto ncols = d.size2();

		a.resize(nrows, ncols, false);

		for (size_t i = 0; i < nrows; ++i) {
			for (size_t j = 0; j < ncols; ++j) {
				a(i, j) = d(i, j).lower();
			}
		}
	}
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (inf != nullptr && sup != nullptr) {
		::mxDestroyArray(inf->value);
		::mxDestroyArray(sup->value);
		inf->value = ::mxDuplicateArray(::mxGetField(dest->value, 0, "inf"));
		sup->value = ::mxDuplicateArray(::mxGetField(dest->value, 0, "sup"));
	} else if (inf == nullptr) {
		::mxDestroyArray(sup->value);
		sup->value = ::mxDuplicateArray(::mxGetField(dest->value, 0, "sup"));
	} else if (sup == nullptr) {
		::mxDestroyArray(inf->value);
		inf->value = ::mxDuplicateArray(::mxGetField(dest->value, 0, "inf"));
	}
#endif
}

void qifen_interval_matrix_get_midrad(const qifen_interval_matrix_t dest, qifen_matrix_t pmid, qifen_matrix_t prad)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	if (pmid != nullptr) {
		**pmid = mid(**dest);
	}
	if (prad != nullptr) {
		**prad = rad(**dest);
	}
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	if (pmid != nullptr) {
		::mxDestroyArray(pmid->value);
		::mxCallMATLAB(1, &pmid->value, 1, const_cast<mxArray**>(&dest->value), "mid");
	}
	if (prad != nullptr) {
		::mxDestroyArray(prad->value);
		::mxCallMATLAB(1, &prad->value, 1, const_cast<mxArray**>(&dest->value), "rad");
	}
#endif
}

void qifen_interval_matrix_get_entry(const qifen_interval_matrix_t v, size_t row, size_t col, qifen_interval_t dest)
{
	double inf, sup;

	::qifen_interval_matrix_get_entry_infsup(v, row, col, &inf, &sup);
	::qifen_interval_set_infsup(dest, inf, sup);
}

void qifen_interval_matrix_get_entry_infsup(const qifen_interval_matrix_t v, size_t row, size_t col, double *inf, double *sup)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	auto &vv = (**v)(row, col);
	if (inf != nullptr)
		*inf = vv.lower();
	if (sup != nullptr)
		*sup = vv.upper();
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	auto pinf = ::mxGetField(v->value, 0, "inf");
	auto psup = ::mxGetField(v->value, 0, "sup");
	auto nrows = ::mxGetM(pinf);
	if (inf != nullptr)
		*inf = ::mxGetPr(pinf)[row + col * nrows];
	if (sup != nullptr)
		*sup = ::mxGetPr(psup)[row + col * nrows];
#endif
}

void qifen_interval_matrix_resize(qifen_interval_matrix_t v, size_t row, size_t col)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	(*v)->resize(row, col);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	auto inf = ::mxCreateDoubleMatrix(row, col, mxREAL);
	auto sup = ::mxCreateDoubleMatrix(row, col, mxREAL);
	auto pinf = ::mxGetPr(inf);
	auto psup = ::mxGetPr(sup);
	auto old_inf = ::mxGetField(v->value, 0, "inf");
	auto old_sup = ::mxGetField(v->value, 0, "sup");
	auto old_pinf = ::mxGetPr(inf);
	auto old_psup = ::mxGetPr(sup);
	auto old_nrows = ::mxGetM(old_inf);
	auto old_ncols = ::mxGetN(old_inf);
	auto m = ::std::min(row, old_nrows);
	auto n = ::std::min(col, old_ncols);

	for (size_t i = 0; i < m; ++i) {
		::std::memcpy(&pinf[i * m], &old_pinf[i * old_ncols], sizeof(double) * n);
		::std::memcpy(&psup[i * m], &old_psup[i * old_ncols], sizeof(double) * n);
	}

	::mxArray *rhs[] = { inf, sup };

	::mxDestroyArray(v->value);
	::mexCallMATLAB(1, &v->value, 2, rhs, "infsup");
#endif
}

void qifen_interval_matrix_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	**dest = -**a;
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);
	::mexCallMATLAB(1, &lhs, 1, const_cast<::mxArray**>(&a->value), "uminus");
#endif
}

void qifen_interval_matrix_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_matrix_kv_add(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_matrix_intlab_add(dest, a, b);
#endif
}

void qifen_interval_matrix_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_matrix_kv_sub(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_matrix_intlab_sub(dest, a, b);
#endif
}

void qifen_interval_matrix_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_matrix_kv_mul(dest, a, b);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_matrix_intlab_mul(dest, a, b);
#endif
}

void qifen_interval_matrix_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
#if defined(QIFEN_CONFIG_INTERVAL_KV)
	::qifen_interval_matrix_kv_inv(dest, a);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_matrix_intlab_inv(dest, a);
#endif
}

#if !defined(QIFEN_CONFIG_DISABLE_KV)
void qifen_interval_matrix_set_entry_kv(qifen_interval_matrix_t dest, size_t row, size_t col, const ::kv::interval<double> &v)
{
	::qifen_interval_matrix_set_entry_infsup(dest, row, col, v.lower(), v.upper());
}

void qifen_interval_matrix_get_entry_kv(const qifen_interval_matrix_t v, size_t row, size_t col, ::kv::interval<double> &dest)
{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
	dest = (**v)(row, col);
#  elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
	::qifen_interval_matrix_get_entry_infsup(v, row, col, &dest.lower(), &dest.upper());
#  endif
}

void qifen_interval_matrix_kv_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
	::qifen_interval_matrix_negate(dest, a);
}

void qifen_interval_matrix_kv_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_KV_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_KV_INTERVAL_MATRIX(bb, b);

	::qifen::internal::interval_matrix_set(dest, aa + bb);
}

void qifen_interval_matrix_kv_add_matrix(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_KV_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_KV_MATRIX(bb, b);

	::qifen::internal::interval_matrix_set(dest, aa + bb);
}

void qifen_interval_matrix_kv_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_KV_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_KV_INTERVAL_MATRIX(bb, b);

	::qifen::internal::interval_matrix_set(dest, aa - bb);
}

void qifen_interval_matrix_kv_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_KV_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_KV_INTERVAL_MATRIX(bb, b);

	::qifen::internal::interval_matrix_set(dest, prod(aa, bb));
}

void qifen_interval_matrix_kv_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
	QIFEN_VAR_KV_INTERVAL_MATRIX(aa, a);
	::boost::numeric::ublas::matrix<::kv::interval<double>> m(aa.size1(), aa.size2());

	::kv::invert(aa, m);
	::qifen::internal::interval_matrix_set(dest, m);
}
#endif

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
int qifen_interval_matrix_set_entry_intlab(qifen_interval_matrix_t dest, size_t row, size_t col, ::mxArray *v)
{
	QIFEN_ENSURE_TYPE("intval", v);
	QIFEN_ENSURE_SCALAR(v);
	::qifen_interval_matrix_set_entry_infsup(
		dest, row, col, ::mxGetScalar(::mxGetField(v, 0, "inf")), ::mxGetScalar(::mxGetField(v, 0, "sup")));
	return 1;
}

void qifen_interval_matrix_intlab_negate(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
	::qifen_interval_matrix_negate(dest, a);
}

void qifen_interval_matrix_intlab_add(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);

	::mxArray *rhs[] = { aa, bb };

	::mexCallMATLAB(1, &lhs, 2, rhs, "plus");
}

void qifen_interval_matrix_intlab_add_matrix(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_matrix_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);
	
	::mxArray *rhs[] = { aa, bb };

	::mexCallMATLAB(1, &lhs, 2, rhs, "plus");
}

void qifen_interval_matrix_intlab_sub(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);

	::mxArray *rhs[] = { aa, bb };

	::mexCallMATLAB(1, &lhs, 2, rhs, "minus");
}

void qifen_interval_matrix_intlab_mul(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a, const qifen_interval_matrix_t b)
{
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(aa, a);
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(bb, b);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);

	::mxArray *rhs[] = { aa, bb };

	::mexCallMATLAB(1, &lhs, 2, rhs, "mtimes");
}

void qifen_interval_matrix_intlab_inv(qifen_interval_matrix_t dest, const qifen_interval_matrix_t a)
{
	QIFEN_VAR_INTLAB_INTERVAL_MATRIX(aa, a);
	QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest);

	::mexCallMATLAB(1, &lhs, 1, &aa, "inv");
}

void qifen_interval_matrix_to_intlab(const qifen_interval_matrix_t v, mxArray **dest)
{
#  if defined(QIFEN_CONFIG_INTERVAL_KV)
	*dest = ::qifen::internal::interval_matrix_to_intlab(v);
#  else
	::qifen_interval_matrix_t r;
	::qifen_interval_matrix_init(r, 1, 1);
	::qifen_interval_matrix_set(r, v);
	*dest = r;
#  endif
}
#endif
